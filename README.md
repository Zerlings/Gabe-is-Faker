# Gabe is Faker

一款用Kotlin/Java编写的知名网游CSGO的开箱模拟软件，采用了Android Jetpack组件与MVVM架构，支持全部31种武器箱和古堡收藏品，完全真实的概率模拟。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1020/150055_6746a3db_512805.png "Screenshot_1571547879_lim[limit].png")

### 项目特点
1. 使用Kotlin编写，基于MVVM架构，采用单Activity+多Fragment模式。
2. 采用自定义LayoutManager控制RecyclerView的滑动。
3. 使用Glide加载图片。
4. 使用Retrofit2获取网络数据。
5. 使用DBFlow实现数据持久化。
6. 使用Navigation管理各Fragment。
7. 使用ViewModel存储和UI关联的数据。
8. 使用LiveData监听数据变化。
9. 依靠Lifecycle实现相关数据生命周期与Fragment的绑定。
10. 使用DataBinding实现数据到UI的自动更新。
11. 使用PermissionsDispatcher管理危险权限。
12. Material Design风格界面。

### UPDATE
V3.3：更新“裂网大行动”武器箱。

### DOWNLOAD
请前往 [酷安](https://www.coolapk.com/apk/180268) 下载

### 使用指南
1. 点击进入想要开的箱子，再单击开始按钮即可。开出物品由真实游戏中的概率决定。实时显示开箱花费与所的物品价格。
2. 有虚拟库存功能，由侧滑菜单进入。库存页面长按可进入复选模式，该模式下可以汰换或删除物品。单选模式下点击右上角可以显示物品磨损。
3. 模拟器页面右上角按钮可以预览箱子包含的物品。在预览页面可以手动添加素材（选中皮肤并点击右下角按钮）。长按皮肤可以切换到暗金版本。


### 已知问题
1. 目前不支持除古堡外的地图收藏品
2. 在部分国产系统上会出现部分UI变形

### UPCOMING
1.  更多自定义功能
