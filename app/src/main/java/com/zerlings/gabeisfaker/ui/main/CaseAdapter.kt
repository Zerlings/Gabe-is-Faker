package com.zerlings.gabeisfaker.ui.main

import com.zerlings.gabeisfaker.data.model.Case
import com.zerlings.gabeisfaker.databinding.CaseItemBinding
import com.zerlings.gabeisfaker.ui.BaseAdapter

class CaseAdapter(brId: Int, layoutId: Int, dataList: MutableList<Case>) : BaseAdapter<Case, CaseItemBinding>(brId, layoutId, dataList)
