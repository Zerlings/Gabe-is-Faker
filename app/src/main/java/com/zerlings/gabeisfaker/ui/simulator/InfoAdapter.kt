package com.zerlings.gabeisfaker.ui.simulator

import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.zerlings.gabeisfaker.R
import com.zerlings.gabeisfaker.data.model.Gun
import com.zerlings.gabeisfaker.databinding.SkinItemBinding
import com.zerlings.gabeisfaker.ui.BaseAdapter

class InfoAdapter(brId: Int, layoutId: Int, dataList: MutableList<Gun>, private val selectedPosition: MutableLiveData<Int>) : BaseAdapter<Gun, SkinItemBinding>(brId, layoutId, dataList) {

    override fun convert(holder: BaseViewHolder, item: Gun, position: Int) {
        super.convert(holder, item, position)
        holder.itemView.setBackgroundColor(ContextCompat.getColor(context, if (selectedPosition.value == position) R.color.selected else R.color.unSelected))
    }
}