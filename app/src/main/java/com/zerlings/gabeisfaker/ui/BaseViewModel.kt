package com.zerlings.gabeisfaker.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel() {

    @JvmOverloads
    protected fun launch(block: suspend () -> Unit, error: suspend (Exception) -> Unit = {}, dispatcher: CoroutineDispatcher = Dispatchers.Main) = viewModelScope.launch(dispatcher) {
        try {
            block()
        } catch (e: Exception) {
            error(e)
        }
    }

}