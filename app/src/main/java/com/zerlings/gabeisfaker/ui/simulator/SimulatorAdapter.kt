package com.zerlings.gabeisfaker.ui.simulator

import com.zerlings.gabeisfaker.data.model.Gun
import com.zerlings.gabeisfaker.databinding.SkinItemBinding
import com.zerlings.gabeisfaker.ui.BaseAdapter

class SimulatorAdapter(brId: Int, layoutId: Int, dataList: MutableList<Gun>) : BaseAdapter<Gun, SkinItemBinding>(brId, layoutId, dataList)

