package com.zerlings.gabeisfaker.ui.main

import com.zerlings.gabeisfaker.MyApplication.Companion.context
import com.zerlings.gabeisfaker.R
import com.zerlings.gabeisfaker.BR
import com.zerlings.gabeisfaker.data.model.Case
import com.zerlings.gabeisfaker.ui.BaseViewModel
import com.zerlings.gabeisfaker.util.initCase

class CaseViewModel : BaseViewModel() {

    val title: String = context.getString(R.string.app_name)

    val caseList: ArrayList<Case> = ArrayList(30)

    val adapter: CaseAdapter = CaseAdapter(BR.mCase, R.layout.case_item, caseList.initCase(true))

    private var isOperation: Boolean = true

    fun changeList() {
        isOperation = !isOperation
        caseList.initCase(isOperation)
    }

}
