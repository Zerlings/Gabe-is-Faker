package com.zerlings.gabeisfaker.ui.simulator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat.START
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.zerlings.gabeisfaker.BR
import com.zerlings.gabeisfaker.R
import com.zerlings.gabeisfaker.databinding.FragmentSimulatorBinding
import com.zerlings.gabeisfaker.ui.BaseFragment
import com.zerlings.gabeisfaker.util.InjectorUtil

class SimulatorFragment : BaseFragment<FragmentSimulatorBinding, SimulatorViewModel>(BR.viewModel, SimulatorViewModel::class.java), View.OnClickListener{

    override fun getLayoutId(): Int = R.layout.fragment_simulator

    override fun initView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) {
        binding.simulatorFooter.footerRightButton.background = ContextCompat.getDrawable(context!!, R.drawable.ic_add)
        /*监测滑动并在期间锁定侧滑菜单*/
        viewModel.isLocked.observe(viewLifecycleOwner, Observer {
            activity.binding.drawerLayout.setDrawerLockMode(if (it) {
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED
            } else {
                DrawerLayout.LOCK_MODE_UNLOCKED
            })
        })
        /*监听并展示滑动结束展示所得物品*/
        binding.draw.simulatorList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        viewModel.onGameEnd()
                        binding.simulatorResult.resultLayout.visibility = View.VISIBLE
                    }
                    else -> {}
                }
            }
        })
        /*切换动画速度模式*/
        binding.switches.switchButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.gameSpeed.value = 5
                binding.switches.animationSpeed.setText(R.string.fast)
            } else {
                viewModel.gameSpeed.value = 1
                binding.switches.animationSpeed.setText(R.string.normal)
            }
        }
        /*简介列表监听*/
        viewModel.infoAdapter.setOnItemClickListener { adapter, view, position, item ->
            when (viewModel.selectedPosition.value) {
                -1 -> {
                    viewModel.selectedPosition.value = position
                    binding.simulatorFooter.footerRightButton.visibility = View.VISIBLE
                }
                position -> {
                    viewModel.selectedPosition.value = -1
                    binding.simulatorFooter.footerRightButton.visibility = View.GONE
                }
                else -> viewModel.selectedPosition.value = position
            }
            viewModel.infoAdapter.notifyDataSetChanged()
        }
        viewModel.infoAdapter.setOnItemLongClickListener { adapter, view, position, item ->
            viewModel.statTrakTransform()
            viewModel.infoAdapter.notifyDataSetChanged()
            false
        }
        /*其余各button设置监听*/
        binding.simulatorHeader.headerLeftButton.setOnClickListener(this)
        binding.simulatorHeader.headerRightButton.setOnClickListener(this)
        binding.startButton.setOnClickListener(this)
        binding.simulatorFooter.footerLeftButton.setOnClickListener(this)
        binding.simulatorFooter.footerRightButton.setOnClickListener(this)
        binding.simulatorResult.discardButton.setOnClickListener(this)
        binding.simulatorResult.keepButton.setOnClickListener(this)
        binding.draw.block.setOnClickListener(this)
    }

    override fun onDestroy() {
        viewModel.onGameDestroy()
        super.onDestroy()
    }

    override fun onBackPressed(): Boolean {
        return when {
            binding.simulatorResult.resultLayout.visibility == View.VISIBLE -> {
                onClick(binding.simulatorResult.discardButton)
                true
            }
            binding.infoView.visibility == View.VISIBLE -> {
                onClick(binding.simulatorHeader.headerRightButton)
                true
            }
            else -> false
        }
    }

    override fun getViewModelFactory(): ViewModelProvider.Factory = InjectorUtil.getSimulatorModelFactory(arguments!!, this)

    /**点击事件集合 */
    override fun onClick(v: View) {
        activity.snackbar?.takeIf { it.isShown }?.apply { dismiss() }
        when (v.id) {
            R.id.discard_button -> {
                /*开箱结束后恢复侧滑菜单及标题栏左右按钮*/
                viewModel.unlockScreen()
                binding.draw.simulatorList.scrollToPosition(0)
                binding.simulatorResult.resultLayout.visibility = View.GONE
            }
            R.id.keep_button -> {
                /*开箱结束后恢复侧滑菜单及标题栏左右按钮*/
                viewModel.saveItem()
                binding.draw.simulatorList.scrollToPosition(0)
                binding.simulatorResult.resultLayout.visibility = View.GONE
            }
            R.id.header_left_button -> activity.binding.drawerLayout.openDrawer(START)
            R.id.footer_left_button -> activity.onBackPressed()
            R.id.header_right_button -> {
                binding.infoView.visibility = if (binding.infoView.visibility == View.GONE) {
                    View.VISIBLE
                }
                else {
                    /*关闭预览页并还原列表*/
                    viewModel.resetInfoList()
                    viewModel.infoAdapter.notifyDataSetChanged()
                    binding.simulatorFooter.footerRightButton.visibility = View.GONE
                    View.GONE
                }
            }
            R.id.footer_right_button -> viewModel.addItem()
            R.id.start_button -> {
                //游戏开始
                binding.draw.simulatorList.smoothScrollToPosition(0)//触发滑动，实际落点由SimulatorLayoutManager控制
                viewModel.onGameStart()
            }
            else -> {}
        }
    }

}
