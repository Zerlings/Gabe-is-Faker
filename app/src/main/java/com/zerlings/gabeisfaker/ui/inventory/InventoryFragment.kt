package com.zerlings.gabeisfaker.ui.inventory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.GravityCompat.START
import com.zerlings.gabeisfaker.BR
import com.zerlings.gabeisfaker.R
import com.zerlings.gabeisfaker.databinding.FragmentInventoryBinding
import com.zerlings.gabeisfaker.ui.BaseFragment

class InventoryFragment : BaseFragment<FragmentInventoryBinding, InventoryViewModel>(BR.viewModel, InventoryViewModel::class.java), View.OnClickListener {

    override fun getLayoutId(): Int = R.layout.fragment_inventory

    override fun initView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) {
        /*替换侧滑菜单*/
        activity.binding.navView.menu.clear()
        activity.binding.navView.inflateMenu(R.menu.sort_menu)
        /*注册监听器*/
        binding.inventoryHeader.headerLeftButton.setOnClickListener(this)
        binding.inventoryHeader.headerRightButton.setOnClickListener(this)
        binding.inventoryFooter.footerLeftButton.setOnClickListener(this)
        binding.inventoryFooter.footerRightButton.setOnClickListener(this)
        binding.inventoryResult.keepButton.setOnClickListener(this)
        binding.inventoryResult.discardButton.setOnClickListener(this)
        /*RecyclerView的item点击事件逻辑*/
        viewModel.adapter.setOnItemClickListener { adapter, view, position, item ->
            viewModel.itemClick(position)
            viewModel.adapter.notifyDataSetChanged()
        }
        viewModel.adapter.setOnItemLongClickListener { adapter, view, position, item ->
            viewModel.itemLongClick()
            false
        }
    }

    override fun onDestroy() {
        activity.binding.navView.menu.clear()
        activity.binding.navView.inflateMenu(R.menu.nav_menu)
        super.onDestroy()
    }

    override fun onBackPressed(): Boolean {
        return when {
            binding.inventoryResult.resultLayout.visibility == View.VISIBLE -> {
                onClick(binding.inventoryResult.discardButton)
                true
            }
            viewModel.positionSet.isNotEmpty() -> {
                viewModel.cancelSelect()
                viewModel.adapter.notifyDataSetChanged()
                true
            }
            else -> false
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.header_left_button -> {
                viewModel.cancelSelect()
                viewModel.adapter.notifyDataSetChanged()
                activity.binding.drawerLayout.openDrawer(START)
            }
            R.id.header_right_button -> {
                if (viewModel.multiMode.value!!) {
                    /*汰换*/
                    if (viewModel.tradeUpCheck()) {
                        viewModel.tradeUpItem()//汰换已选
                        binding.inventoryResult.resultLayout.visibility = View.VISIBLE
                    }
                } else {
                    /*查询磨损*/
                    Toast.makeText(context, "Float: " + viewModel.uniqueItem.wearValue/100, Toast.LENGTH_SHORT).show()
                }
            }//汰换（多选）或查询（单选）
            R.id.footer_left_button -> activity.onBackPressed()
            R.id.footer_right_button -> viewModel.deleteItem()//删除（多选）
            R.id.keep_button -> {
                viewModel.saveItem()
                viewModel.adapter.notifyDataSetChanged()
                binding.inventoryResult.resultLayout.visibility = View.GONE
            }
            R.id.discard_button -> binding.inventoryResult.resultLayout.visibility = View.GONE
        }
    }

    /**显示筛选过后的列表*/
    fun showSpecificList(level: Int) {
        viewModel.setSpecificList(level)
        viewModel.adapter.notifyDataSetChanged()
    }

}
