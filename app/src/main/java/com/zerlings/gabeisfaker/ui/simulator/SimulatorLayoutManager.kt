package com.zerlings.gabeisfaker.ui.simulator

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.zerlings.gabeisfaker.util.dip2px
import java.util.*

class SimulatorLayoutManager(private val context: Context) : LinearLayoutManager(context) {

    private val random = Random()

    private var scrollSpeed = 1

    override fun smoothScrollToPosition(recyclerView: RecyclerView, state: RecyclerView.State?, position: Int) {
        val linearSmoothScroller = object : LinearSmoothScroller(recyclerView.context) {

            /*返回减速状态下的滑动时间*/
            override fun calculateTimeForDeceleration(dx: Int): Int {
                return 9000/scrollSpeed
            }

            /*返回总的滑动距离*/
            override fun calculateDtToFit(viewStart: Int, viewEnd: Int, boxStart: Int, boxEnd: Int, snapPreference: Int): Int {
                val randomX = 147*random.nextFloat()//计算随机落点
                return -dip2px(5530f + randomX)
            }
        }
        linearSmoothScroller.targetPosition = position
        startSmoothScroll(linearSmoothScroller)
    }

    fun setSpeed(sp: Int) {
        scrollSpeed = sp
    }
}
