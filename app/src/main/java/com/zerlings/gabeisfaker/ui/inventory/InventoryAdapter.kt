package com.zerlings.gabeisfaker.ui.inventory

import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.zerlings.gabeisfaker.R
import com.zerlings.gabeisfaker.data.model.UniqueItem
import com.zerlings.gabeisfaker.databinding.SkinItemBinding
import com.zerlings.gabeisfaker.ui.BaseAdapter

class InventoryAdapter(brId: Int, layoutId: Int, dataList: MutableList<UniqueItem>, private val positionSet: HashSet<Int>) : BaseAdapter<UniqueItem, SkinItemBinding>(brId, layoutId, dataList) {

    override fun convert(holder: BaseViewHolder, item: UniqueItem, position: Int) {
        super.convert(holder, item, position)
        holder.getBinding<SkinItemBinding>()?.itemLayout?.setBackgroundColor(ContextCompat.getColor(context, if (positionSet.contains(position)) R.color.selected else R.color.unSelected))
    }
}