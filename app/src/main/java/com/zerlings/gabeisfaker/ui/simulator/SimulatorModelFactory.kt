package com.zerlings.gabeisfaker.ui.simulator

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner

class SimulatorModelFactory(private val arguments: Bundle, owner: SavedStateRegistryOwner, defaultArgs: Bundle?) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    @Suppress("UNCHECKED_CAST")
    override fun <TModel : ViewModel?> create(key: String, modelClass: Class<TModel>, handle: SavedStateHandle): TModel {
        return SimulatorViewModel(arguments, handle) as TModel
    }
}