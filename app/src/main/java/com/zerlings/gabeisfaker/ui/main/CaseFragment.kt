package com.zerlings.gabeisfaker.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.GravityCompat.START
import androidx.navigation.Navigation
import com.zerlings.gabeisfaker.BR
import com.zerlings.gabeisfaker.R
import com.zerlings.gabeisfaker.databinding.FragmentCaseBinding
import com.zerlings.gabeisfaker.ui.BaseFragment

class CaseFragment : BaseFragment<FragmentCaseBinding, CaseViewModel>(BR.viewModel, CaseViewModel::class.java) {

    override fun getLayoutId(): Int = R.layout.fragment_case

    override fun initView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) {
        viewModel.adapter.setOnItemClickListener { adapter, view, position, item ->
            activity.snackbar?.takeIf { it.isShown }?.dismiss()?: let {
                val mCase = viewModel.caseList[position]
                Navigation.findNavController(view).navigate(R.id.action_case_fragment_to_simulator_fragment,
                        Bundle().apply {
                            putString("case_name", mCase.caseName)
                            putString("case_image_name", mCase.imageName)
                            putStringArray("rare_item_type", mCase.rareItemType)
                            putStringArray("rare_skin_type", mCase.rareSkinType)
                            putString("market_hash_name", mCase.marketHashName)
                            putString("key_hash_name", mCase.keyHashName)
                        })
            }
        }

        binding.caseHeader.headerLeftButton.setOnClickListener {
            activity.snackbar?.takeIf { it.isShown }?.apply{ dismiss() }
            activity.binding.drawerLayout.openDrawer(START)
        }

        binding.caseHeader.headerRightButton.setOnClickListener {
            viewModel.changeList()
            viewModel.adapter.notifyDataSetChanged()
        }
    }

    override fun onBackPressed(): Boolean {
        return false
    }

}
