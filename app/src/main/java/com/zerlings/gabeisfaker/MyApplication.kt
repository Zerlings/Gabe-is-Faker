package com.zerlings.gabeisfaker

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.dbflow5.config.FlowConfig
import com.dbflow5.config.FlowManager

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
        FlowManager.init(FlowConfig.builder(this)
            .openDatabasesOnInit(true)
            .build())
    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context

    }
}