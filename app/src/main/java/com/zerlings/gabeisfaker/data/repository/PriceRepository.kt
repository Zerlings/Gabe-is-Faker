package com.zerlings.gabeisfaker.data.repository

import com.google.gson.Gson
import com.zerlings.gabeisfaker.MyApplication
import com.zerlings.gabeisfaker.data.model.PriceBundle
import com.zerlings.gabeisfaker.data.retrofit.NetworkManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

object PriceRepository : BaseRepository() {

    private val networkManager = NetworkManager

    suspend fun loadPriceData() = withContext(Dispatchers.IO){
        File(MyApplication.context.getFileStreamPath("allprices").absolutePath).readText()
    }

    suspend fun storePriceData(response: PriceBundle?) = withContext(Dispatchers.IO){
        File(MyApplication.context.getFileStreamPath("allprices").absolutePath).writeText(Gson().toJson(response))
    }

    suspend fun queryAllPrices() = apiCall { networkManager.queryAllPrices() }
}