package com.zerlings.gabeisfaker.data.db

import com.dbflow5.config.database
import com.dbflow5.query.list
import com.dbflow5.query.result
import com.dbflow5.query.select
import com.dbflow5.structure.delete
import com.dbflow5.structure.insert
import com.dbflow5.transaction.fastUpdate
import com.zerlings.gabeisfaker.data.model.*

object UniqueItemDao {

    fun getAllItems() = (select from UniqueItem::class.java).list as ArrayList<UniqueItem>

    fun updateAllItems(itemList: ArrayList<UniqueItem>) = itemList.fastUpdate().build().execute(database<AppDatabase>())

    fun saveItem(item: UniqueItem) = item.insert()

    fun deleteItem(item: UniqueItem) = item.delete()

    fun getGlove(skinType: String) = (select from Glove::class
            where (Glove_Table.skinName.eq(skinType)))
            .result

    fun getKnife(skinType: String, knifeType: String) = (select from Knife::class
            where (Knife_Table.knifeName.eq(knifeType))
            and (Knife_Table.skinName.eq(skinType)))
            .result
}