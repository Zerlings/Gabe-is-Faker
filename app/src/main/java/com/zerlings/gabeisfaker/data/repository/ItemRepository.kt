package com.zerlings.gabeisfaker.data.repository

import com.zerlings.gabeisfaker.data.db.UniqueItemDao
import com.zerlings.gabeisfaker.data.model.UniqueItem

object ItemRepository : BaseRepository()  {

    private val uniqueItemDao = UniqueItemDao

    fun getUniqueItems() = uniqueItemDao.getAllItems()

    fun updateUniqueItems(itemList: ArrayList<UniqueItem>) = uniqueItemDao.updateAllItems(itemList)

    fun saveItem(item: UniqueItem) = uniqueItemDao.saveItem(item)

    fun deleteItem(item: UniqueItem) = uniqueItemDao.deleteItem(item)

    fun getGlove(skinType: String) = uniqueItemDao.getGlove(skinType)

    fun getKnife(skinType: String, knifeType: String) = uniqueItemDao.getKnife(skinType, knifeType)
}