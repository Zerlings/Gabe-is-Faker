package com.zerlings.gabeisfaker.data.model

import android.os.Parcel
import android.os.Parcelable

class Stats() : Parcelable{
    var rareCount = 0
    var convertCount = 0
    var classifiedCount = 0
    var restrictedCount = 0
    var milspecCount = 0
    var industrialCount = 0
    var consumerCount = 0
    var totalCount = 0
    var obtainedValue = "0"
    var totalCost = "0"

    constructor(parcel: Parcel) : this() {
        rareCount = parcel.readInt()
        convertCount = parcel.readInt()
        classifiedCount = parcel.readInt()
        restrictedCount = parcel.readInt()
        milspecCount = parcel.readInt()
        industrialCount = parcel.readInt()
        consumerCount = parcel.readInt()
        totalCount = parcel.readInt()
        obtainedValue = parcel.readString() ?: "0"
        totalCost = parcel.readString() ?: "0"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(rareCount)
        parcel.writeInt(convertCount)
        parcel.writeInt(classifiedCount)
        parcel.writeInt(restrictedCount)
        parcel.writeInt(milspecCount)
        parcel.writeInt(industrialCount)
        parcel.writeInt(consumerCount)
        parcel.writeInt(totalCount)
        parcel.writeString(obtainedValue)
        parcel.writeString(totalCost)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Stats> {
        override fun createFromParcel(parcel: Parcel): Stats {
            return Stats(parcel)
        }

        override fun newArray(size: Int): Array<Stats?> {
            return arrayOfNulls(size)
        }
    }

}
