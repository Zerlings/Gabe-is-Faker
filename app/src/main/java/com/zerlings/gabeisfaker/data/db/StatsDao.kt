package com.zerlings.gabeisfaker.data.db

import android.content.Context.MODE_PRIVATE
import com.zerlings.gabeisfaker.MyApplication.Companion.context
import com.zerlings.gabeisfaker.data.model.Stats

object StatsDao {

    /**SharedPreferences统计数据暂存*/
    fun storeStats(stats: Stats) {
        val editor = context.getSharedPreferences("stats", MODE_PRIVATE).edit()
        editor.putInt("rare", stats.rareCount)
        editor.putInt("convert", stats.convertCount)
        editor.putInt("classified", stats.classifiedCount)
        editor.putInt("restricted", stats.restrictedCount)
        editor.putInt("milspec", stats.milspecCount)
        editor.putInt("industrial", stats.industrialCount)
        editor.putInt("consumer", stats.consumerCount)
        editor.putInt("total", stats.totalCount)
        editor.putString("obtained", stats.obtainedValue)
        editor.putString("cost", stats.totalCost)
        editor.apply()
    }

    /**SharedPreferences统计数据恢复*/
    fun restoreStats(): Stats {
        val preferences = context.getSharedPreferences("stats", MODE_PRIVATE)
        val stats = Stats()
        stats.rareCount = preferences.getInt("rare", 0)
        stats.convertCount = preferences.getInt("convert", 0)
        stats.classifiedCount = preferences.getInt("classified", 0)
        stats.restrictedCount = preferences.getInt("restricted", 0)
        stats.milspecCount = preferences.getInt("milspec", 0)
        stats.industrialCount = preferences.getInt("industrial", 0)
        stats.consumerCount = preferences.getInt("consumer", 0)
        stats.totalCount = preferences.getInt("total", 0)
        stats.obtainedValue = preferences.getString("obtained", "0")!!
        stats.totalCost = preferences.getString("cost", "0")!!
        return stats
    }

    /**重置统计数据*/
    fun resetStats() {
        val editor = context.getSharedPreferences("stats", MODE_PRIVATE).edit()
        editor.putInt("rare", 0)
        editor.putInt("convert", 0)
        editor.putInt("classified", 0)
        editor.putInt("restricted", 0)
        editor.putInt("milspec", 0)
        editor.putInt("industrial", 0)
        editor.putInt("consumer", 0)
        editor.putInt("total", 0)
        editor.putString("obtained", "0")
        editor.putString("cost", "0")
        editor.apply()
    }
}