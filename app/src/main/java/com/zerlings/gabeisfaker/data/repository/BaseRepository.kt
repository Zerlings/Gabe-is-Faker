package com.zerlings.gabeisfaker.data.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class BaseRepository {

    protected suspend fun <T: Any> apiCall(call: suspend() -> T) : T {
        return withContext(Dispatchers.IO) { call.invoke() }.apply {
            // 特殊处理

        }
    }

    protected suspend fun <T: Any> singleExec(exec: suspend() -> Boolean) : Boolean {
        return withContext(Dispatchers.IO) { exec.invoke() }.apply {
            // 特殊处理

        }
    }

    protected suspend fun <T: Any, M: Collection<T>> collectionExec(exec: suspend() -> Long) : Long {
        return withContext(Dispatchers.IO) { exec.invoke() }.apply {
            // 特殊处理

        }
    }
}