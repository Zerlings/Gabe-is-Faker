package com.zerlings.gabeisfaker.data.model

import com.dbflow5.annotation.Column
import com.dbflow5.annotation.PrimaryKey
import com.dbflow5.annotation.Table
import com.zerlings.gabeisfaker.MyApplication
import com.zerlings.gabeisfaker.R
import com.zerlings.gabeisfaker.data.db.AppDatabase
import com.zerlings.gabeisfaker.ui.MainViewModel
import java.util.*

@Table(database = AppDatabase::class)
class UniqueItem() {

    @PrimaryKey(autoincrement = true)
    var id: Int = 0

    @Column
    var itemName: String = ""

    @Column
    var skinName: String = ""

    @Column
    var marketHashName: String = ""

    @Column
    var price: String = "0"

    @Column
    var imageName: String = ""

    @Column
    var quality: Int = 0

    @Column
    var exterior: String = ""

    @Column
    var wearValue: Float = 0.toFloat()

    @Column
    var isStatTrak: Boolean = false

    @Column
    var caseName: String = ""

    @Column
    var itemType: Int = 0

    constructor(gun: Gun): this(gun, null)

    constructor(gun: Gun, wearValue: Float?): this() {
        this.itemName = gun.gunName
        this.skinName = gun.skinName
        this.quality = gun.quality
        this.imageName = gun.imageName
        this.isStatTrak = gun.isStatTrak
        this.itemType = 0
        this.caseName = gun.caseName
        val random = Random()
        this.wearValue = wearValue ?: random.nextFloat() * (gun.maxWear - gun.minWear) + gun.minWear
        when {
            this.wearValue >= 0 && this.wearValue < 7 -> {
                this.exterior = MyApplication.context.resources.getString(R.string.factory_new)
                this.marketHashName = "${gun.gunName} | ${gun.skinNameMHN} (${MyApplication.context.resources.getString(R.string.factory_new_mhn)})"
            }
            this.wearValue >= 7 && this.wearValue < 15 -> {
                this.exterior = MyApplication.context.resources.getString(R.string.minimal_wear)
                this.marketHashName = "${gun.gunName} | ${gun.skinNameMHN} (${MyApplication.context.resources.getString(R.string.minimal_wear_mhn)})"
            }
            this.wearValue >= 15 && this.wearValue < 38 -> {
                this.exterior = MyApplication.context.resources.getString(R.string.field_tested)
                this.marketHashName = "${gun.gunName} | ${gun.skinNameMHN} (${MyApplication.context.resources.getString(R.string.field_tested_mhn)})"
            }
            this.wearValue >= 38 && this.wearValue < 45 -> {
                this.exterior = MyApplication.context.resources.getString(R.string.well_worn)
                this.marketHashName = "${gun.gunName} | ${gun.skinNameMHN} (${MyApplication.context.resources.getString(R.string.well_worn_mhn)})"
            }
            else -> {
                this.exterior = MyApplication.context.resources.getString(R.string.battle_scarred)
                this.marketHashName = "${gun.gunName} | ${gun.skinNameMHN} (${MyApplication.context.resources.getString(R.string.battle_scarred_mhn)})"
            }
        }
        if(isStatTrak) {
            this.marketHashName = "StatTrak™ $marketHashName"
        }
        this.price = MainViewModel.priceMap[this.marketHashName] ?: price
    }

    constructor(knife: Knife): this() {
        this.itemName = knife.knifeName
        this.skinName = knife.skinName
        this.quality = 6
        this.imageName = knife.imageName
        this.itemType = 1
        val random = Random()
        this.isStatTrak = random.nextInt(10) == 0
        this.marketHashName = if (isStatTrak) "★ StatTrak™" else "★"
        this.wearValue = random.nextFloat() * (knife.maxWear - knife.minWear) + knife.minWear
        when {
            knife.skinName == MyApplication.context.getString(R.string.vanilla) -> {
                this.exterior = ""
                this.skinName = ""
                this.marketHashName = "$marketHashName ${knife.knifeNameMHN}"
            }
            wearValue >= 0 && wearValue < 7 -> {
                this.exterior = MyApplication.context.getString(R.string.factory_new)
                this.marketHashName = "$marketHashName ${knife.knifeNameMHN} | ${knife.skinNameMHN} (${MyApplication.context.resources.getString(R.string.factory_new_mhn)})"
            }
            wearValue >= 7 && wearValue < 15 -> {
                this.exterior = MyApplication.context.getString(R.string.minimal_wear)
                this.marketHashName = "$marketHashName ${knife.knifeNameMHN} | ${knife.skinNameMHN} (${MyApplication.context.resources.getString(R.string.minimal_wear_mhn)})"
            }
            wearValue >= 15 && wearValue < 38 -> {
                this.exterior = MyApplication.context.getString(R.string.field_tested)
                this.marketHashName = "$marketHashName ${knife.knifeNameMHN} | ${knife.skinNameMHN} (${MyApplication.context.resources.getString(R.string.field_tested_mhn)})"
            }
            wearValue >= 38 && wearValue < 45 -> {
                this.exterior = MyApplication.context.getString(R.string.well_worn)
                this.marketHashName = "$marketHashName ${knife.knifeNameMHN} | ${knife.skinNameMHN} (${MyApplication.context.resources.getString(R.string.well_worn_mhn)})"
            }
            else -> {
                this.exterior = MyApplication.context.getString(R.string.battle_scarred)
                this.marketHashName = "$marketHashName ${knife.knifeNameMHN} | ${knife.skinNameMHN} (${MyApplication.context.resources.getString(R.string.battle_scarred_mhn)})"
            }
        }
        this.price = MainViewModel.priceMap[this.marketHashName] ?: price
    }

    constructor(glove: Glove): this() {
        this.itemName = glove.gloveName
        this.skinName = glove.skinName
        this.quality = 7
        this.imageName = glove.imageName
        this.itemType = 1
        val random = Random()
        this.wearValue = random.nextFloat() * (glove.maxWear - glove.minWear) + glove.minWear
        when {
            wearValue >= 0 && wearValue < 7 -> {
                this.exterior = MyApplication.context.getString(R.string.factory_new)
                this.marketHashName = "★ ${glove.gloveNameMHN} | ${glove.skinNameMHN} (${MyApplication.context.resources.getString(R.string.factory_new_mhn)})"
            }
            wearValue >= 7 && wearValue < 15 -> {
                this.exterior = MyApplication.context.getString(R.string.minimal_wear)
                this.marketHashName = "★ ${glove.gloveNameMHN} | ${glove.skinNameMHN} (${MyApplication.context.resources.getString(R.string.minimal_wear_mhn)})"
            }
            wearValue >= 15 && wearValue < 38 -> {
                this.exterior = MyApplication.context.getString(R.string.field_tested)
                this.marketHashName = "★ ${glove.gloveNameMHN} | ${glove.skinNameMHN} (${MyApplication.context.resources.getString(R.string.field_tested_mhn)})"
            }
            wearValue >= 38 && wearValue < 45 -> {
                this.exterior = MyApplication.context.getString(R.string.well_worn)
                this.marketHashName = "★ ${glove.gloveNameMHN} | ${glove.skinNameMHN} (${MyApplication.context.resources.getString(R.string.well_worn_mhn)})"
            }
            else -> {
                this.exterior = MyApplication.context.getString(R.string.battle_scarred)
                this.marketHashName = "★ ${glove.gloveNameMHN} | ${glove.skinNameMHN} (${MyApplication.context.resources.getString(R.string.battle_scarred_mhn)})"
            }
        }
        this.isStatTrak = false
        this.price = MainViewModel.priceMap[this.marketHashName] ?: price
    }

}
