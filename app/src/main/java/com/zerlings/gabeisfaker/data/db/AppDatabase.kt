package com.zerlings.gabeisfaker.data.db

import com.dbflow5.annotation.Database
import com.dbflow5.annotation.Migration
import com.dbflow5.config.DBFlowDatabase
import com.dbflow5.database.DatabaseWrapper
import com.dbflow5.migration.BaseMigration
import com.dbflow5.transaction.fastInsert
import com.dbflow5.transaction.fastSave
import com.zerlings.gabeisfaker.util.initGlove
import com.zerlings.gabeisfaker.util.initKnife

@Database(version = 11)
abstract class AppDatabase: DBFlowDatabase() {

    @Migration(version = 0, database = AppDatabase::class)
    class Migration0 : BaseMigration() {

        override fun migrate(database: DatabaseWrapper) {
            initKnife().fastInsert().build().execute(database)
            initGlove().fastInsert().build().execute(database)
        }
    }

    @Migration(version = 8, database = AppDatabase::class)
    class Migration1 : BaseMigration() {

        override fun migrate(database: DatabaseWrapper) {

            database.execSQL("drop table if exists UniqueItem")
            database.execSQL("create table UniqueItem ("
                    + "id integer primary key autoincrement, "
                    + "itemName text, "
                    + "skinName text, "
                    + "marketHashName text, "
                    + "price text, "
                    + "imageName text, "
                    + "quality integer, "
                    + "exterior text, "
                    + "wearValue real, "
                    + "isStatTrak integer default 0, "
                    + "caseName text, "
                    + "itemType integer)")

            database.execSQL("drop table if exists Knife")
            database.execSQL("drop table if exists Glove")
            database.execSQL("create table Knife ("
                    + "knifeName text, "
                    + "skinName text, "
                    + "knifeNameMHN text, "
                    + "skinNameMHN text, "
                    + "imageName text primary key, "
                    + "minWear integer, "
                    + "maxWear integer)")
            database.execSQL("create table Glove ("
                    + "gloveName text, "
                    + "skinName text, "
                    + "gloveNameMHN text, "
                    + "skinNameMHN text, "
                    + "imageName text primary key, "
                    + "minWear integer, "
                    + "maxWear integer)")
        }
    }

    @Migration(version = 11, database = AppDatabase::class)
    class Migration2 : BaseMigration() {

        override fun migrate(database: DatabaseWrapper) {

            initKnife().fastSave().build().execute(database)
            initGlove().fastSave().build().execute(database)
        }
    }
}
