package com.zerlings.gabeisfaker.data.model

import com.dbflow5.annotation.Column
import com.dbflow5.annotation.PrimaryKey
import com.dbflow5.annotation.Table
import com.zerlings.gabeisfaker.data.db.AppDatabase

@Table(database = AppDatabase::class)
class Knife (@Column var knifeName: String = "",
             @Column var skinName: String = "",
             @Column var knifeNameMHN: String = "",
             @Column var skinNameMHN: String = "",
             @PrimaryKey var imageName: String = "",
             @Column var minWear: Int = 0,
             @Column var maxWear: Int = 100)



