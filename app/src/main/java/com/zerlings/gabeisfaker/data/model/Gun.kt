package com.zerlings.gabeisfaker.data.model

class Gun(){

    var gunName: String = ""

    var skinName: String = ""

    var skinNameMHN: String = ""

    var imageName: String = ""

    var quality: Int = 0

    var minWear: Int = 0

    var maxWear: Int = 0

    var caseName: String = ""

    var isStatTrak: Boolean = false


    constructor(gunName: String, skinName: String, skinNameMHN: String, imageName: String, quality: Int, minWear: Int, maxWear: Int, caseName: String): this() {
        this.gunName = gunName
        this.skinName = skinName
        this.skinNameMHN = skinNameMHN
        this.imageName = imageName
        this.quality = quality
        this.minWear = minWear
        this.maxWear = maxWear
        this.caseName = caseName
    }
}
