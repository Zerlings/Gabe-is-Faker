package com.zerlings.gabeisfaker.data.repository

import com.zerlings.gabeisfaker.data.db.StatsDao
import com.zerlings.gabeisfaker.data.model.Stats

object StatsRepository : BaseRepository()  {

    private val statsDao = StatsDao

    fun resetStats() = statsDao.resetStats()

    fun storeStats(stats: Stats) = statsDao.storeStats(stats)

    fun restoreStats(): Stats = statsDao.restoreStats()
}