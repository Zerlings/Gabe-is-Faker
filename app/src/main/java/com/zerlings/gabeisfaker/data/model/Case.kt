package com.zerlings.gabeisfaker.data.model

class Case (var caseName: String,
            var imageName: String,
            var marketHashName: String,
            var keyHashName: String,
            var rareItemType: Array<String>,
            var rareSkinType: Array<String>)
