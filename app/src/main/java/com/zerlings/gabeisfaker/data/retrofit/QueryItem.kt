package com.zerlings.gabeisfaker.data.retrofit

import com.zerlings.gabeisfaker.data.model.PriceBundle
import retrofit2.http.GET

interface QueryItem {

    @GET("Zerlings/CSGOItemPrice/raw/master/allprices.json")
    suspend fun queryAllPrices(): PriceBundle

}
