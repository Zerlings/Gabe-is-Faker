package com.zerlings.gabeisfaker.util

import android.os.Bundle
import androidx.savedstate.SavedStateRegistryOwner
import com.zerlings.gabeisfaker.ui.simulator.SimulatorModelFactory

object InjectorUtil {

    @JvmOverloads fun getSimulatorModelFactory(arguments: Bundle, owner: SavedStateRegistryOwner, defaultArgs: Bundle? = null) = SimulatorModelFactory(arguments, owner, defaultArgs)

}