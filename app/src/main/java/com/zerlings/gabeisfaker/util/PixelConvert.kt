package com.zerlings.gabeisfaker.util

import com.zerlings.gabeisfaker.MyApplication

/**
 * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
 */
fun dip2px(dpValue: Float): Int {
    val scale = MyApplication.context.resources.displayMetrics.density
    return (dpValue * scale + 0.5f).toInt()
}

/**
 * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
 */
fun px2dip(pxValue: Float): Int {
    val scale = MyApplication.context.resources.displayMetrics.density
    return (pxValue / scale + 0.5f).toInt()
}
