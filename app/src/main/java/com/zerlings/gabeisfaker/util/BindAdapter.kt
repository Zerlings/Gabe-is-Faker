package com.zerlings.gabeisfaker.util

import android.animation.LayoutTransition
import android.animation.ObjectAnimator
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zerlings.gabeisfaker.R
import com.zerlings.gabeisfaker.ui.BaseAdapter
import com.zerlings.gabeisfaker.ui.GridItemDecoration
import com.zerlings.gabeisfaker.ui.MainActivity
import com.zerlings.gabeisfaker.ui.inventory.InventoryAdapter
import com.zerlings.gabeisfaker.ui.main.CaseAdapter
import com.zerlings.gabeisfaker.ui.simulator.InfoAdapter
import com.zerlings.gabeisfaker.ui.simulator.SimulatorAdapter
import com.zerlings.gabeisfaker.ui.simulator.SimulatorItemDecoration
import com.zerlings.gabeisfaker.ui.simulator.SimulatorLayoutManager

@BindingAdapter("adapter")
fun <T, TBinding: ViewDataBinding> RecyclerView.setAdapterAndManager(adapter: BaseAdapter<T, TBinding>) {
    when (adapter) {
        is CaseAdapter -> layoutManager = GridLayoutManager(context, 4)
        is SimulatorAdapter -> {
            layoutManager = SimulatorLayoutManager(context!!).apply {
                setSpeed(1)
                orientation = LinearLayoutManager.HORIZONTAL
            }
            val spacingInPixels = dip2px(10f)//设置item间隔
            addItemDecoration(SimulatorItemDecoration(spacingInPixels))
            val header = LayoutInflater.from(context).inflate(R.layout.item_header, this, false)//添加表头
            adapter.setHeaderView(header, 0, LinearLayoutManager.HORIZONTAL)
        }
        is InfoAdapter -> {
            val spacingInPixels = (resources.displayMetrics.widthPixels - dip2px(600f))/10
            addItemDecoration(GridItemDecoration(spacingInPixels))
            setPadding(spacingInPixels)
            layoutManager = GridLayoutManager(context, 4)
        }
        is InventoryAdapter -> {
            val spacingInPixels = (resources.displayMetrics.widthPixels - dip2px(600f))/10
            addItemDecoration(GridItemDecoration(spacingInPixels))
            setPadding(spacingInPixels,0,spacingInPixels,0)
            layoutManager = GridLayoutManager(context, 4)
        }
        else -> {}
    }
    this.adapter = adapter
}

@BindingAdapter("speed")
fun RecyclerView.setSpeed(speed: Int) {
    (layoutManager as? SimulatorLayoutManager)?.setSpeed(speed)
}

@BindingAdapter(value = ["transitionType", "transitionStart"], requireAll = true)
fun ViewGroup.setTransition(transitionType: String, transitionStart: String) {
    val transition = LayoutTransition()
    val distance: Float
    val direction: String
    if (transitionStart == "top" || transitionStart == "bottom") {
        direction = "translationY"
        distance = resources.displayMetrics.heightPixels.toFloat()
    } else {
        direction = "translationX"
        distance = resources.displayMetrics.widthPixels.toFloat()
    }
    val from = when (transitionStart) {
        "top" -> -distance
        "right" -> distance
        "bottom" -> distance
        "left" -> -distance
        else -> 0f
    }
    val to = 0f
    if (transitionType == "in" || transitionType == "both") {
        val appearAnim = ObjectAnimator.ofFloat(this, direction, from, to)
        appearAnim.duration = transition.getDuration(LayoutTransition.APPEARING)
        transition.setAnimator(LayoutTransition.APPEARING, appearAnim)
    }
    if (transitionType == "out" || transitionType == "both") {
        val disappearAnim = ObjectAnimator.ofFloat(this, direction, to, from)
        disappearAnim.duration = transition.getDuration(LayoutTransition.DISAPPEARING)
        transition.setAnimator(LayoutTransition.DISAPPEARING, disappearAnim)
    }
    layoutTransition = transition
}

@BindingAdapter("imageName")
fun ImageView.setImage(imageName: String?) {
    imageName?.let {
        val imageId = resources.getIdentifier(it, "drawable", "com.zerlings.gabeisfaker")
        Glide.with(context).load(imageId).into(this)
    }
}

@BindingAdapter("layoutColor")
fun RelativeLayout.setColor(quality: Int) {

    when (quality) {
        MainActivity.LEVEL_RARE -> setBackgroundColor(ContextCompat.getColor(context, R.color.rare))
        MainActivity.LEVEL_EXTRAORDINARY -> setBackgroundColor(ContextCompat.getColor(context, R.color.convert))
        MainActivity.LEVEL_CONVERT -> setBackgroundColor(ContextCompat.getColor(context, R.color.convert))
        MainActivity.LEVEL_CLASSIFIED -> setBackgroundColor(ContextCompat.getColor(context, R.color.classified))
        MainActivity.LEVEL_RESTRICTED -> setBackgroundColor(ContextCompat.getColor(context, R.color.restricted))
        MainActivity.LEVEL_MILSPEC -> setBackgroundColor(ContextCompat.getColor(context, R.color.milspec))
        MainActivity.LEVEL_INDUSTRIAL -> setBackgroundColor(ContextCompat.getColor(context, R.color.industrial))
        MainActivity.LEVEL_CONSUMER -> setBackgroundColor(ContextCompat.getColor(context, R.color.consumer))
        else -> {}
    }
}

@BindingAdapter("backgroundImage")
fun Button.setBackgroundImage(multiMode: Boolean?) {
    when (multiMode) {
        true -> background = ContextCompat.getDrawable(context, R.drawable.ic_switch)
        false -> background = ContextCompat.getDrawable(context, R.drawable.ic_query)
        else -> {}
    }
}

